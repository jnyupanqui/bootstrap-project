// Modal events
var openModalBtn;
$('#bookingModal').on('show.bs.modal', function(){
    console.log('El modal comienza a abrirse...');

    // Disable button
    openModalBtn = event.target;
    $(openModalBtn).addClass('bg-secondary');
    $(openModalBtn).prop('disabled', true);
});

$('#bookingModal').on('shown.bs.modal', function(){
    console.log('El modal terminó de abrir.');
});

$('#bookingModal').on('hide.bs.modal', function(){
    console.log('El modal comienza a ocultarse...');
});

$('#bookingModal').on('hidden.bs.modal', function(){
    console.log('El modal se terminó de ocultar.');

    // Enable button
    $(openModalBtn).removeClass('bg-secondary');
    $(openModalBtn).prop('disabled', false);
});

// Vary the contents of the modal depending on which button was clicked
$('#bookingModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var hotalName = button.data('whatever');

    var modal = $(this);
    modal.find('.modal-body #inputHotel').val(hotalName);
});