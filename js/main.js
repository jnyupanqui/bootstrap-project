$('#btn-popover').popover(
    {
        trigger: 'hover'
    }
);

$('#btn-tooltip').tooltip(
    {
        trigger: 'hover'
    }
);

$('.carousel').carousel(
    {
        interval: 2000
    }
);