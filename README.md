# Bootstrap Project

This repository contain a responsive web design using Bootstrap UI Framework with Flex on a node.js environment. 

> This web page was developed during the *'Diseñando páginas web con Bootstrap'* course in the *'Full Stack Web Development Specialization'* by Universidad Austral through Coursera.

## Requirements
You will need:

- [Nodejs](https://nodejs.org/)
- Npm
- [Ruby](https://github.com/gruntjs/grunt-contrib-sass)
- Grunt-CLI ```npm intall grunt-cli -g```
- [Sass](https://sass-lang.com/install) ```npm install sass -g```

## Usage

- Clone the repository ```git clone https://jnyupanqui@bitbucket.org/jnyupanqui/bootstrap-project.git```
- Go to the project directory
- Install the dependencies ```npm install```
- Run the project in dev```grunt```

### For production
- Generates the artifact ```grunt build```
- Start server in prod profile ```grunt prod```
